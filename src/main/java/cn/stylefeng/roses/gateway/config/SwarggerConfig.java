package cn.stylefeng.roses.gateway.config;

import cn.stylefeng.roses.gateway.core.constants.AuthConstants;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: roses-gateway
 * @description: 接口文档
 * @author: Mr.Liu
 * @create: 2020-04-22 14:07
 **/
@Component
@Primary
public class SwarggerConfig implements SwaggerResourcesProvider {
    //添加不进行接口验证的
    static {
        AuthConstants.SwarggerNoCheck.put("/roses-system/v2/api-docs",true);
        AuthConstants.SwarggerNoCheck.put("/saltice-iot/v2/api-docs",true);
        AuthConstants.SwarggerNoCheck.put("/v2/api-docs",true);
        AuthConstants.SwarggerNoCheck.put("/saltice-ms/v2/api-docs",true);
        AuthConstants.SwarggerNoCheck.put("/saltice-sc/v2/api-docs",true);
    }

    @Override
    public List<SwaggerResource> get() {
        List resources = new ArrayList<>();
        // member-service ：自定义
        // /api-member/v2/api-docs：固定 ，其中/=/api-member是网关配置中的path
        //用户服务
        resources.add(swaggerResource("roses-system", "/roses-system/v2/api-docs", "2.0"));
        //盆栽服务
        resources.add(swaggerResource("saltice-iot", "/saltice-iot/v2/api-docs", "2.0"));
        //药柜服务
        resources.add(swaggerResource("saltice-ms", "/saltice-ms/v2/api-docs", "2.0"));
        //社交服务
        resources.add(swaggerResource("saltice-sc", "/saltice-sc/v2/api-docs", "2.0"));
        //当前网关服务
        resources.add(swaggerResource("gateway", "/v2/api-docs", "2.0"));
        return resources;
    }

    private SwaggerResource swaggerResource(String name, String location, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }
}
