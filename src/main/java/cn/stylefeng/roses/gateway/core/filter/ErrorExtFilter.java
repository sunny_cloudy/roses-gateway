package cn.stylefeng.roses.gateway.core.filter;

/**
 * @program: roses-gateway
 * @description:
 * @author: Mr.Liu
 * @create: 2020-04-13 10:28
 **/

import cn.stylefeng.roses.core.reqres.response.ResponseData;
import cn.stylefeng.roses.core.util.SpringContextHolder;
import cn.stylefeng.roses.gateway.core.constants.ZuulFiltersOrder;
import cn.stylefeng.roses.kernel.logger.api.log.LogTask;
import com.alibaba.fastjson.JSONObject;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.post.SendErrorFilter;
import org.springframework.stereotype.Component;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import java.io.IOException;

/**
 * 从后置(post)中抛出的异常，使用该过滤器返回错误信息
 * @author liuyanbin
 */
public class ErrorExtFilter extends SendErrorFilter {
    private static final Logger logger = LoggerFactory.getLogger(ErrorExtFilter.class);
    @Override
    public String filterType() {
        return "error";
    }
    @Override
    public int filterOrder() {
        return ZuulFiltersOrder.ERROR_FILTER_ORDER_F;
    }
    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        //4.再次debug zuul的源码发现，在error-filter执行之后，程序还会执行SendErrorFilter（zuul自带的filter，用于整合返回数据之类的，filterOrder为0）
        //5.在SendErrorFilter中会处理ZuulException（此异常在spring-cloud-zuul-ratelimit的源码中抛出），并重新设置response(例如StatusCode之类)
        //6.这导致了error-filter设置的response失效，所以在error-filter中需要去除ZuulException
        Object e = ctx.getThrowable();
        if (e instanceof ZuulException) {
             ctx.remove("throwable");
        }
        //这个ctx.set("sendErrorFilter.ran", true);也是可以不使用sendErrorFilter过滤，但是得自己写下面的代码
//        RequestDispatcher dispatcher = ctx.getRequest().getRequestDispatcher("/error");
//        if (dispatcher != null) {
//            //这个也是可以不使用sendErrorFilter过滤，但是得自己写下面的代码
//            ctx.set("sendErrorFilter.ran", true);
//            if (!ctx.getResponse().isCommitted()) {
//                ctx.setResponseStatusCode(500);
//                try {
//                    dispatcher.forward(ctx.getRequest(), ctx.getResponse());
//                } catch (ServletException | IOException e) {
//                    ctx.set("sendErrorFilter.ran", false);
//                }
//            }
//        }
        return null;
    }
}
