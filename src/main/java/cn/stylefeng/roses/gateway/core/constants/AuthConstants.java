/**
 * Copyright 2018-2020 stylefeng & fengshuonan (sn93@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.stylefeng.roses.gateway.core.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 鉴权相关的常量
 *
 * @author fengshuonan
 * @date 2018-08-10 23:30
 */
public interface AuthConstants {

    /**
     * 鉴权请求头名称
     */
    String AUTH_HEADER = "Authorization";

    /**
     * 鉴权地址
     */
    String AUTH_ACTION_URL = "/gatewayAction/auth";
    /**
     * 注册地址
     */
    String AUTH_REGISTER_URL = "/gatewayAction/register";
    /**
     * 盆栽app用户注册地址
     */
    String POT_AUTH_REGISTER_URL = "/gatewayAction/app/register";
    /**
     * 盆栽app用户更新信息地址
     */
    String POT_AUTH_UPDATEINFO_URL = "/gatewayAction/app/updateInfo";
    /**
     * 盆栽app用户更新密码地址
     */
    String POT_AUTH_UPDATEPWD_URL = "/gatewayAction/app/updatePassword";
    /**
     * 盆栽app用户更新简介地址
     */
    String POT_INTRODUCTION_UPDATEPWD_URL = "/gatewayAction/app/updateIntroduction";
    /**
     * 盆栽app用户头像信息地址
     */
    String POT_AUTH_AVATAR_URL = "/gatewayAction/app/Avatar";
    /**
     * 检验token是否正确
     */
    String VALIDATE_TOKEN_URL = "/gatewayAction/validateToken";

    /**
     * 退出接口
     */
    String LOGOUT_URL = "/gatewayAction/logout";
    /**
     * 通过token获取登陆用户信息
     */
    String GET_USER_INFO = "/gatewayAction/getUser";
    /**
     * 接口文档不需要拦截的
     */
    public  static Map<String,Boolean> SwarggerNoCheck = new HashMap<>();
    /**
     * 其他服务不需要拦截的
     */
    public  static Map<String,Boolean> OtherServerNoCheck = new HashMap<>();
}
