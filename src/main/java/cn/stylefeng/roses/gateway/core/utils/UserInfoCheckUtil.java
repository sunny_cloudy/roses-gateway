package cn.stylefeng.roses.gateway.core.utils;

import cn.stylefeng.roses.core.reqres.response.ResponseData;
import cn.stylefeng.roses.kernel.model.api.base.AppUserWarpper;

/**
 * @program: roses-gateway
 * @description: 用户信息检查类
 * @author: Mr.Liu
 * @create: 2020-04-20 11:11
 **/
public class UserInfoCheckUtil {
    /**
     * 检查 用户名、密码、手机号
     * @param appLogin
     * @return
     */
    public static ResponseData registerCheck(AppUserWarpper appLogin){
        if(appLogin.getName()==null ||appLogin.getPassword()==null||appLogin.getPhone()==null){
            return ResponseData.error("注册失败，信息不完整");
        }
        if("".equals(appLogin.getName()) ||"".equals(appLogin.getPassword())||"".equals(appLogin.getPhone())){
            return ResponseData.error("注册失败，信息不完整");
        }
        return null;
    }
}
