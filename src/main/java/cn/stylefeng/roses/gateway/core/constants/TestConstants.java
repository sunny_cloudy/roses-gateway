package cn.stylefeng.roses.gateway.core.constants;

/**
 * @program: roses-gateway
 * @description: 测试的接口路径，用于取消拦截的路径
 * @author: Mr.Liu
 * @create: 2020-02-22 10:55
 **/

public interface TestConstants {

    /**
     * 鉴权地址
     */
    String AUTH_ACTION_URL = "/ljobin-medicine-cab/test/selectOne";
    /**
     * 药柜地址
     */
    String MC_ACTION_URL = "/ljobin-ms-kf/test/selectOne";
}
