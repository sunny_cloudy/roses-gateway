package cn.stylefeng.roses.gateway.core.filter;

import cn.stylefeng.roses.core.reqres.response.ResponseData;
import cn.stylefeng.roses.core.util.SpringContextHolder;
import cn.stylefeng.roses.kernel.logger.api.log.LogTask;
import cn.stylefeng.roses.kernel.model.exception.ServiceException;
import com.alibaba.fastjson.JSONObject;
import com.netflix.zuul.FilterProcessor;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @program: roses-gateway
 * @description:
 * @author: Mr.Liu
 * @create: 2020-04-13 10:25
 **/
public class DidiFilterProcessor extends FilterProcessor {
    private static final Logger logger = LoggerFactory.getLogger(DidiFilterProcessor.class);
    private volatile static LogTask logTask;
    public LogTask getLogTask(){
        if(logTask==null){
            synchronized (LogTask.class){
                if(logTask==null){
                    logTask = SpringContextHolder.getBean(LogTask.class);
                }
            }
        }
        return logTask;
    }
    @Override
    public Object processZuulFilter(ZuulFilter filter) throws ZuulException {
        try {
            return super.processZuulFilter(filter);
        } catch (ZuulException e) {
            RequestContext ctx = RequestContext.getCurrentContext();
            ctx.set("failed.exception", e.getCause().getMessage());
            ctx.set("failed.filter", filter);
            if("com.netflix.zuul.exception.ZuulException: Forwarding error".equals(e.getCause().getMessage())){
                ctx.setResponseBody(JSONObject.toJSONString(ResponseData.error("服务繁忙，请稍后再试")));
            }else if(e.getCause() instanceof ServiceException){
                ctx.setResponseBody(JSONObject.toJSONString(ResponseData.error(((ServiceException)e.getCause()).getCode(),e.getCause().getMessage())));
            }else {
                ctx.setResponseBody(JSONObject.toJSONString(ResponseData.error(e.nStatusCode,e.getCause().getMessage())));
            }
            ctx.getResponse().setContentType("application/json;charset=UTF-8");
            logger.error("===>【error】ServletPath：{}，failed.exception：{}，failed.filter：{}",ctx.getRequest().getServletPath(),e.getCause().getMessage(),filter);
            getLogTask().exceptionLog(new Exception(e.getCause()));
            //发生错误会在SendErrorFilter这个拦截器中返回给客户端
            throw e;
            //return null;
        }
    }
}
