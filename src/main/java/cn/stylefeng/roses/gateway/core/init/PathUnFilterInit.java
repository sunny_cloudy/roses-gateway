package cn.stylefeng.roses.gateway.core.init;

import cn.stylefeng.roses.gateway.core.constants.AuthConstants;
import cn.stylefeng.roses.gateway.modular.entity.UnIntercept;
import cn.stylefeng.roses.gateway.modular.service.UnInterceptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import java.util.List;

/**
 * @program: roses-gateway
 * @description: 不需要拦截的地址初始化
 * @author: Mr.Liu
 * @create: 2020-06-30 14:00
 **/
@Component
public class PathUnFilterInit implements ApplicationRunner {
    @Autowired
    private UnInterceptService unInterceptService;
    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<UnIntercept> unInterceptList =  unInterceptService.queryAll();
        synchronized (AuthConstants.OtherServerNoCheck){
            AuthConstants.OtherServerNoCheck.clear();
            unInterceptList.forEach(v->{
                if(!"".equals(v.getUrl()))
                    AuthConstants.OtherServerNoCheck.put(v.getUrl(), v.getEnable() > 0);
            });
        }
    }
}
