package cn.stylefeng.roses.gateway;

import cn.stylefeng.roses.core.util.SpringContextHolder;
import cn.stylefeng.roses.gateway.core.filter.DidiFilterProcessor;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.netflix.zuul.FilterProcessor;
import com.spring4all.swagger.EnableSwagger2Doc;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 网关服务
 *
 * @author fengshuonan
 * @Date 2017/11/10 上午11:24
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, DruidDataSourceAutoConfigure.class},scanBasePackages = {"cn.stylefeng.roses"})
@EnableFeignClients(basePackages = "cn.stylefeng.roses.gateway.modular.consumer")
@EnableZuulProxy
@EnableDiscoveryClient
@EnableSwagger2Doc
@MapperScan(basePackages="cn.stylefeng.roses.gateway.modular.dao.mapper")
@EnableScheduling
public class RosesGatewayApplication {

    public static void main(String[] args) {
        FilterProcessor.setProcessor(new DidiFilterProcessor());
        SpringApplication.run(RosesGatewayApplication.class, args);
    }

}
