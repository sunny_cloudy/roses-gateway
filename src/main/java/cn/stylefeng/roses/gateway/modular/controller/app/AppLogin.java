package cn.stylefeng.roses.gateway.modular.controller.app;

import cn.stylefeng.roses.core.reqres.response.ResponseData;
import cn.stylefeng.roses.core.reqres.response.SuccessResponseData;
import cn.stylefeng.roses.gateway.core.constants.AuthConstants;
import cn.stylefeng.roses.gateway.core.utils.UserInfoCheckUtil;
import cn.stylefeng.roses.gateway.modular.consumer.AuthServiceConsumer;
import cn.stylefeng.roses.gateway.modular.service.TokenValidateService;
import cn.stylefeng.roses.kernel.model.api.base.AppUserWarpper;
import cn.stylefeng.roses.kernel.model.api.base.RpcBaseResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;

/**
 * @program: roses-gateway
 * @description: app用户登陆
 * @author: Mr.Liu
 * @create: 2020-04-17 15:45
 **/
@RestController
@Api("app用户登陆")
public class AppLogin {
    @Autowired
    private TokenValidateService tokenValidateService;
    @Autowired
    private AuthServiceConsumer authServiceConsumer;
    /**
     * 注册接口
     */
    @RequestMapping(value = AuthConstants.POT_AUTH_REGISTER_URL,method = RequestMethod.POST)
    @ApiOperation("注册接口")
    public ResponseData register(AppUserWarpper appLogin) throws ParseException {
        ResponseData o = UserInfoCheckUtil.registerCheck(appLogin);
        if(o!=null){
            return o;
        }
        RpcBaseResponse token = authServiceConsumer.register(appLogin);
        return new SuccessResponseData(token,true);
    }
    /**
     * 更新用户信息接口
     */
    @ApiOperation("更新用户信息接口")
    @RequestMapping(value = AuthConstants.POT_AUTH_UPDATEINFO_URL,method = RequestMethod.POST)
    public ResponseData updateInfo( AppUserWarpper appLogin,HttpServletRequest request) throws ParseException {
        String token = tokenValidateService.getTokenFromRequest02(request);
        RpcBaseResponse response = authServiceConsumer.updateLoginUserByToken(token,appLogin);
        return new SuccessResponseData(response,true);
    }
    /**
     * 更新头像
     */
    @ApiOperation("更新头像")
    @RequestMapping(value = AuthConstants.POT_AUTH_AVATAR_URL,method = RequestMethod.POST,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseData updateAvatar(@ApiParam(value = "头像", required = true) @RequestPart("avatar") MultipartFile avatar, HttpServletRequest request) throws ParseException {
        if(avatar.isEmpty()){
            return ResponseData.error("未获取到头像数据");
        }
        String token = tokenValidateService.getTokenFromRequest02(request);
        RpcBaseResponse response = authServiceConsumer.updateAvatar(avatar,token);
        return new SuccessResponseData(response,true);
    }
}
