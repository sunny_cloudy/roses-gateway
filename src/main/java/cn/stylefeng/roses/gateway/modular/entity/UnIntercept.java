package cn.stylefeng.roses.gateway.modular.entity;

import java.io.Serializable;

/**
 * 不拦截列表(UnIntercept)实体类
 *
 * @author makejava
 * @since 2020-06-30 13:55:36
 */
public class UnIntercept implements Serializable {
    private static final long serialVersionUID = -73594711971139525L;
    
    private Integer id;
    /**
    * 不拦截的url
    */
    private String url;
    /**
     * 别名
     */
    private String alias;
    /**
    * 是否可用，0-不可用，1-可用
    */
    private Integer enable;

    public UnIntercept() {
    }

    public UnIntercept(String url, String alias) {
        this.url = url;
        this.alias = alias;
        this.enable = 1;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

}