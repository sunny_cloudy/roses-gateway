package cn.stylefeng.roses.gateway.modular.consumer.fallback;

import cn.ljobin.bibi.api.IotDataApi;
import cn.ljobin.bibi.domain.Torrent;
import cn.stylefeng.roses.gateway.modular.consumer.IotServiceConsumer;
import cn.stylefeng.roses.kernel.logger.api.common.annotion.IotLog;
import cn.stylefeng.roses.kernel.logger.api.common.state.LogType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @program: roses-gateway
 * @description:
 * @author: Mr.Liu
 * @create: 2020-04-12 16:56
 **/
@RequestMapping("/iot/api")
@Component
public class IotServiceFallback implements IotServiceConsumer {
    @Override
    @IotLog(value = "服务繁忙",type = LogType.FEIGN_DEFEAT)
    public List<Torrent> getTorrentList(Torrent torrent) {
        return null;
    }
}
