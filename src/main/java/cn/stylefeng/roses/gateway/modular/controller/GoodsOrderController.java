package cn.stylefeng.roses.gateway.modular.controller;

import cn.stylefeng.roses.core.util.ToolUtil;
import cn.stylefeng.roses.gateway.modular.consumer.GoodsOrderConsumer;
import com.stylefeng.roses.api.order.model.GoodsOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: roses-gateway
 * @description:
 * @author: Mr.Liu
 * @create: 2020-04-09 18:30
 **/
@RestController
public class GoodsOrderController {
    @Autowired
    private GoodsOrderConsumer goodsOrderConsumer;

    @RequestMapping(value = "/findOrderById",method = RequestMethod.POST)
    public GoodsOrder findOrderById(@RequestParam("orderId") Long orderId){
        if (ToolUtil.isEmpty(orderId)) {
            return null;
        } else {
            return goodsOrderConsumer.findOrderById(orderId);
        }
    }
}
