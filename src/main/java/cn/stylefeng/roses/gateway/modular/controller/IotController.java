package cn.stylefeng.roses.gateway.modular.controller;

import cn.ljobin.bibi.api.IotDataApi;
import cn.ljobin.bibi.domain.Torrent;
import cn.stylefeng.roses.gateway.core.exception.base.AuthExceptionEnum;
import cn.stylefeng.roses.gateway.modular.consumer.IotServiceConsumer;
import cn.stylefeng.roses.kernel.model.exception.AbstractBaseExceptionEnum;
import cn.stylefeng.roses.kernel.model.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * @program: roses-gateway
 * @description:
 * @author: Mr.Liu
 * @create: 2020-04-11 17:36
 **/
@RestController
@RequestMapping("/iot")
public class IotController {
    @Autowired
    private IotServiceConsumer iotServiceConsumer;
    @RequestMapping(value = "getTorrent",method = RequestMethod.POST)
    public List<Torrent> getTorrentList(@RequestBody Torrent torrent) {
        return iotServiceConsumer.getTorrentList(torrent);
    }
    @RequestMapping(value = "getTorrent2",method = RequestMethod.GET)
    public List<Torrent> getTorrentList2() {
        Torrent t = new Torrent();
        t.setId((long)807);
        t.setImei("862105021586131");
        t.setUser_name("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMjQ4MTgyMzgxNzE0NjMyNzA2IiwiaWF0IjoxNTg2NjgxMTM1LCJleHAiOjE1ODY3NjM5MzV9.8FIhiGXHCXlAtEkFhxGxkccZ8i1Yr579IkG_DXeN18NO8HNZc0bC8_6KbalJia-WoszOFxPfREnRez7un1j9aA");
        //t.setExdate(new Date());
        return iotServiceConsumer.getTorrentList(t);
    }
}
