package cn.stylefeng.roses.gateway.modular.consumer;

import cn.ljobin.bibi.api.IotDataApi;
import cn.stylefeng.roses.gateway.modular.consumer.fallback.IotServiceFallback;
import com.ljobin.bibi.api.service.TestService;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @program: roses-gateway
 * @description: iot服务的消费者
 * @author: Mr.Liu
 * @create: 2020-02-22 10:53
 **/

@FeignClient(value = "saltice-iot" , fallback = IotServiceFallback.class)
public interface IotServiceConsumer extends IotDataApi {
}
