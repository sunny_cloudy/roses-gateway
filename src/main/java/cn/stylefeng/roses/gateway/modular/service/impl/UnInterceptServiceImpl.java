package cn.stylefeng.roses.gateway.modular.service.impl;

import cn.stylefeng.roses.gateway.modular.entity.UnIntercept;
import cn.stylefeng.roses.gateway.modular.dao.UnInterceptDao;
import cn.stylefeng.roses.gateway.modular.service.UnInterceptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 不拦截列表(UnIntercept)表服务实现类
 *
 * @author makejava
 * @since 2020-06-30 13:55:38
 */
@Service("unInterceptService")
public class UnInterceptServiceImpl implements UnInterceptService {
    @Autowired(required = false)
    private UnInterceptDao unInterceptDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public UnIntercept queryById(Integer id) {
        return this.unInterceptDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<UnIntercept> queryAllByLimit(int offset, int limit) {
        return this.unInterceptDao.queryAllByLimit(offset, limit);
    }

    @Override
    public List<UnIntercept> queryAll() {
        return this.unInterceptDao.queryAlls();
    }

    /**
     * 新增数据
     *
     * @param unIntercept 实例对象
     * @return 实例对象
     */
    @Override
    public int insert(UnIntercept unIntercept) {
        return this.unInterceptDao.insert(unIntercept);
    }

    /**
     * 修改数据
     *
     * @param unIntercept 实例对象
     * @return 实例对象
     */
    @Override
    public int update(UnIntercept unIntercept) {
        return this.unInterceptDao.update(unIntercept);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.unInterceptDao.deleteById(id) > 0;
    }
    /**
     * 通过url删除数据
     *
     * @param url url
     * @return 是否成功
     */
    @Override
    public boolean deleteByUrl(String url) {
        return this.unInterceptDao.deleteByUrl(url) > 0;
    }
}