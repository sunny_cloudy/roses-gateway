package cn.stylefeng.roses.gateway.modular.dao;

import cn.stylefeng.roses.gateway.modular.entity.UnIntercept;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.Mapping;

import java.util.List;

/**
 * 不拦截列表(UnIntercept)表数据库访问层
 *
 * @author makejava
 * @since 2020-06-30 13:55:37
 */
@Mapper
public interface UnInterceptDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    UnIntercept queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<UnIntercept> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);
    /**
     * 查询所有数据
     *
     * @return 对象列表
     */
    List<UnIntercept> queryAlls();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param unIntercept 实例对象
     * @return 对象列表
     */
    List<UnIntercept> queryAll(UnIntercept unIntercept);

    /**
     * 新增数据
     *
     * @param unIntercept 实例对象
     * @return 影响行数
     */
    int insert(UnIntercept unIntercept);

    /**
     * 修改数据
     *
     * @param unIntercept 实例对象
     * @return 影响行数
     */
    int update(UnIntercept unIntercept);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);
    /**
     * 通过url删除数据
     *
     * @param url url
     * @return 是否成功
     */
    int deleteByUrl(String url);

}