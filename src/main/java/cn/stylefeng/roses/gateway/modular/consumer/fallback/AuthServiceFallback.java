package cn.stylefeng.roses.gateway.modular.consumer.fallback;

import cn.stylefeng.roses.gateway.modular.consumer.AuthServiceConsumer;
import cn.stylefeng.roses.kernel.logger.api.common.annotion.IotLog;
import cn.stylefeng.roses.kernel.logger.api.common.state.LogType;
import cn.stylefeng.roses.kernel.model.api.AuthService;
import cn.stylefeng.roses.kernel.model.api.base.AppUserWarpper;
import cn.stylefeng.roses.kernel.model.api.base.RpcBaseResponse;
import cn.stylefeng.roses.kernel.model.auth.AbstractLoginUser;
import cn.stylefeng.roses.kernel.model.auth.SysUser;
import cn.stylefeng.roses.kernel.model.enums.SystemMenu;
import cn.stylefeng.roses.system.api.context.LoginUser;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * @program: roses-gateway
 * @description:
 * @author: Mr.Liu
 * @create: 2020-04-12 16:52
 **/
@RequestMapping("/auth/api")
@Component
public class AuthServiceFallback implements AuthServiceConsumer {

    @Override
    @IotLog(value = "注册失败",type = LogType.FEIGN_DEFEAT)
    public RpcBaseResponse register(AppUserWarpper appUserWarpper) {
        return RpcBaseResponse.error("服务繁忙");
    }

    @Override
    @IotLog(value = "更新头像失败",type = LogType.FEIGN_DEFEAT)
    public RpcBaseResponse updateAvatar(MultipartFile files, String token) {
        return RpcBaseResponse.error("服务繁忙");
    }

    @Override
    @IotLog(value = "登陆失败",type = LogType.FEIGN_DEFEAT)
    public RpcBaseResponse login(String account, String password, SystemMenu systemMenu) {
        return RpcBaseResponse.error("服务繁忙");
    }

    @Override
    @IotLog(value = "检查token失败",type = LogType.FEIGN_DEFEAT)
    public boolean checkToken(String token) {
        return false;
    }

    @Override
    @IotLog(value = "退出登陆失败",type = LogType.FEIGN_DEFEAT)
    public void logout(String token) {

    }

    @Override
    @IotLog(value = "获取用户信息失败",type = LogType.FEIGN_DEFEAT)
    public RpcBaseResponse getLoginUserByToken(String token) {
        return RpcBaseResponse.error("服务繁忙");
    }

    @Override
    @IotLog(value = "获取用户信息失败",type = LogType.FEIGN_DEFEAT)
    public SysUser getUserByToken(String token) {
        return null;
    }

    @Override
    public Long getLoginUserIdByToken(String token) {
        return null;
    }

    @Override
    @IotLog(value = "通过token更新用户失败",type = LogType.FEIGN_DEFEAT)
    public RpcBaseResponse updateLoginUserByToken(String token, AppUserWarpper userWarpper) {
        return RpcBaseResponse.error("服务繁忙");
    }

    @Override
    @IotLog(value = "通过token更新用户密码失败",type = LogType.FEIGN_DEFEAT)
    public RpcBaseResponse updatePasswordByToken(String token, String password) {
        return RpcBaseResponse.error("服务繁忙");
    }
}
