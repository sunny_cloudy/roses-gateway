package cn.stylefeng.roses.gateway.modular.controller;

import cn.stylefeng.roses.core.reqres.response.ResponseData;
import cn.stylefeng.roses.gateway.core.constants.AuthConstants;
import cn.stylefeng.roses.gateway.modular.entity.UnIntercept;
import cn.stylefeng.roses.gateway.modular.service.UnInterceptService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @program: roses-gateway
 * @description:
 * @author: Mr.Liu
 * @create: 2020-06-30 14:45
 **/
@RestController
@RequestMapping("/unIntercept")
@Slf4j
@Api("url拦截")
public class UnInterceptController {
    @Autowired
    private UnInterceptService unInterceptService;

    /**
     * 刷新不需要拦截的url
     * @return
     */
    @GetMapping("/refresh")
    @ApiOperation("刷新不需要拦截的url")
    public ResponseData refresh(){
        List<UnIntercept> unInterceptList =  unInterceptService.queryAll();
        synchronized (AuthConstants.OtherServerNoCheck){
            AuthConstants.OtherServerNoCheck.clear();
            unInterceptList.forEach(v->{
                if(!"".equals(v.getUrl()))
                    AuthConstants.OtherServerNoCheck.put(v.getUrl(), v.getEnable() > 0);
            });
        }
        return ResponseData.success();
    }
    /**
     * 添加不需要拦截的url
     * @return
     */
    @GetMapping("/add")
    @ApiOperation("添加不需要拦截的url")
    public ResponseData add(@RequestParam("url") @NotBlank(message = "请输入url") @Length(min = 1,message = "url不合法")@ApiParam("url") String url,
                            @RequestParam("alias") @NotBlank(message = "请输入别名") @ApiParam("别名") String alias){
        UnIntercept unIntercept = new UnIntercept(url,alias);
        try{
            unInterceptService.insert(unIntercept);
        }catch (Throwable t){
            unInterceptService.update(unIntercept);
            log.info("当前url：{}，已经存在了，更新状态成功",url);
        }
        AuthConstants.OtherServerNoCheck.put(url, true);
        return ResponseData.success();
    }
    /**
     * 删除 不需要拦截的url
     * @return
     */
    @GetMapping("/cancel")
    @ApiOperation("取消 不需要拦截的url")
    public ResponseData cancel(@RequestParam("url") @NotBlank(message = "请输入url") @Length(min = 1,message = "url不合法")@ApiParam("url") String url){
        unInterceptService.deleteByUrl(url);
        AuthConstants.OtherServerNoCheck.remove(url);
        return ResponseData.success();
    }
}
