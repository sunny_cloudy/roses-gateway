/**
 * Copyright 2018-2020 stylefeng & fengshuonan (sn93@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.stylefeng.roses.gateway.modular.controller;

import cn.stylefeng.roses.core.reqres.response.ResponseData;
import cn.stylefeng.roses.core.reqres.response.SuccessResponseData;
import cn.stylefeng.roses.core.util.CheckSystem;
import cn.stylefeng.roses.core.util.ToolUtil;
import cn.stylefeng.roses.gateway.core.constants.AuthConstants;
import cn.stylefeng.roses.gateway.modular.consumer.AuthServiceConsumer;
import cn.stylefeng.roses.kernel.model.api.base.RpcBaseResponse;
import cn.stylefeng.roses.kernel.model.exception.ServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

import static cn.stylefeng.roses.gateway.core.constants.AuthConstants.AUTH_HEADER;

/**
 * 登录控制器
 *
 * @author fengshuonan
 * @date 2017-11-08-下午7:04
 */
@RestController
@Api("app用户信息操作")
public class LoginController {

    @Autowired
    private AuthServiceConsumer authServiceConsumer;

//    /**
//     * 注册接口
//     */
//    @RequestMapping(value = AuthConstants.AUTH_REGISTER_URL,method = RequestMethod.POST)
//    public ResponseData register(@RequestParam("account") String account,@RequestParam("password") String password,
//                             @RequestParam("name")String name,
////                             @RequestParam("name")String name, @RequestParam("birthday")Date birthday,
//                             @RequestParam("sex")String sex, @RequestParam("email")String email,
//                             @RequestParam("phone")String phone) {
//        Date birthday = new Date();
//        RpcBaseResponse token = authServiceConsumer.register(account, password, name, birthday, sex, email, phone);
//        return new SuccessResponseData(token,true);
//    }
    /**
     * 登录接口
     */
    @ApiOperation("登录接口")
    @RequestMapping(value = AuthConstants.AUTH_ACTION_URL,method = RequestMethod.POST)
    public ResponseData auth(@ApiParam(value = "account和password", required = true) @RequestBody Map<String, String> auth, HttpServletRequest request) {
        String userName = auth.get("account");
        String password = auth.get("password");
        if(userName==null||password==null){
            return ResponseData.error(400,"请输入正确信息");
        }
        if("".equals(userName)||"".equals(password)){
            return ResponseData.error(400,"请输入正确信息");
        }
        RpcBaseResponse token =authServiceConsumer.login(userName, password, CheckSystem.check(request));
        return new SuccessResponseData(token,true);
    }
    /**
     * 验证token是否正确
     */
    @ApiOperation("验证token是否正确")
    @RequestMapping(value = AuthConstants.VALIDATE_TOKEN_URL,method = RequestMethod.POST)
    public ResponseData validateToken(@ApiParam(value = "token", required = true) @RequestParam("token") String token) {
        boolean tokenFlag = authServiceConsumer.checkToken(token);
        return ResponseData.success(tokenFlag);
    }

    /**
     * 退出接口
     */
    @ApiOperation("退出接口")
    @RequestMapping(value = AuthConstants.LOGOUT_URL,method = RequestMethod.POST)
    public ResponseData logout(@ApiParam(value = "token", required = true)@RequestParam("token") String token) {
        authServiceConsumer.logout(token);
        return ResponseData.success();
    }
    /**
     * 获取登陆用户,在这个网关里面会出现解析json错误，我们直接通过微服务向授权服务直接发起获取用户信息即可
     */
    @RequestMapping(value = AuthConstants.GET_USER_INFO,method = RequestMethod.POST)
    @ApiOperation("获取当前登陆用户信息")
    public ResponseData getUser(@ApiParam(value = "token", required = true)@RequestParam("token") String token) {
        RpcBaseResponse response = authServiceConsumer.getLoginUserByToken(token);
        return ResponseData.success(response.getCode(),response.getToken(),response.getData());
    }
    /**
     * 更新密码
     */
    @ApiOperation("更新密码")
    @RequestMapping(value = AuthConstants.POT_AUTH_UPDATEPWD_URL,method = RequestMethod.POST)
    public ResponseData updatePassword(@RequestHeader(AUTH_HEADER) String token ,@ApiParam(value = "新密码", required = true)@RequestParam("password") String password) {
        if (ToolUtil.isEmpty(token)) {
            return ResponseData.error("未获取到登陆信息");
        }
        if(token.length()<="Bearer ".length()){
            return ResponseData.error("未获取到登陆信息");
        }
        token = token.substring("Bearer ".length());
        RpcBaseResponse response = authServiceConsumer.updatePasswordByToken(token,password);
        return ResponseData.success(response.getCode(),response.getToken(),response.getData());
    }
    /**
     * 更新简介
     */
    @ApiOperation("更新简介")
    @RequestMapping(value = AuthConstants.POT_INTRODUCTION_UPDATEPWD_URL,method = RequestMethod.POST)
    public ResponseData updateIntroduction(@RequestHeader(AUTH_HEADER) String token ,@ApiParam(value = "新简介", required = true)@RequestParam("introduction") String introduction) {
        if (ToolUtil.isEmpty(token)) {
            return ResponseData.error("未获取到登陆信息");
        }
        if(token.length()<="Bearer ".length()){
            return ResponseData.error("未获取到登陆信息");
        }
        token = token.substring("Bearer ".length());
        RpcBaseResponse response = authServiceConsumer.updateIntroductionByToken(token,introduction);
        return ResponseData.success(response.getCode(),response.getToken(),response.getData());
    }
}
