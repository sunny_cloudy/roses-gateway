package cn.stylefeng.roses.gateway.modular.service;

import cn.stylefeng.roses.gateway.modular.entity.UnIntercept;
import java.util.List;

/**
 * 不拦截列表(UnIntercept)表服务接口
 *
 * @author makejava
 * @since 2020-06-30 13:55:38
 */
public interface UnInterceptService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    UnIntercept queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<UnIntercept> queryAllByLimit(int offset, int limit);
    /**
     * 查询所有数据
     *
     * @return 对象列表
     */
    List<UnIntercept> queryAll();
    /**
     * 新增数据
     *
     * @param unIntercept 实例对象
     * @return 实例对象
     */
    int insert(UnIntercept unIntercept);

    /**
     * 修改数据
     *
     * @param unIntercept 实例对象
     * @return 实例对象
     */
    int update(UnIntercept unIntercept);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);
    /**
     * 通过url删除数据
     *
     * @param url url
     * @return 是否成功
     */
    boolean deleteByUrl(String url);
}