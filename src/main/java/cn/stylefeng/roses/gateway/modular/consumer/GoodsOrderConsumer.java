package cn.stylefeng.roses.gateway.modular.consumer;

import com.stylefeng.roses.api.order.GoodsOrderApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @program: roses-gateway
 * @description: 商品订单
 * @author: Mr.Liu
 * @create: 2020-04-09 18:28
 **/
@FeignClient("roses-order")
public interface GoodsOrderConsumer extends GoodsOrderApi {
}
